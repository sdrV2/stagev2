#!/bin/bash

R_PATH="src/main/resources/INPUT/Generated/R"
S_PATH="src/main/resources/INPUT/Generated/S"
OUTPUT_HIST_LOCAL="src/main/resources/OUTPUT/MainHistStandard/Generated"
JAR_PATH="target/stagecleanV2-1.0-SNAPSHOT.jar"
HDFS_PORT="hdfs://localhost:9000/"
INPUT="/input"
OUTPUT_HIST_DFS="/output_hist"

MAIN_CLASS="MainHistStandard"

## Prepare
#hdfs namenode -format
hadoop fs -rm -r $OUTPUT_HIST_DFS
hadoop fs -rm -r $INPUT
hadoop fs -mkdir $INPUT

if [[ $R_PATH != "" ]]; then
  hadoop fs -put $R_PATH $INPUT
fi

if [[ $S_PATH != "" ]]; then
  hadoop fs -put $S_PATH $INPUT
fi

hadoop jar $JAR_PATH $MAIN_CLASS $INPUT $OUTPUT_HIST_DFS $HDFS_PORT

if [[ $OUTPUT_HIST_LOCAL != "" ]]; then
  hadoop fs -getmerge $OUTPUT_HIST_DFS $OUTPUT_HIST_LOCAL
fi
