#!/bin/bash

R_PATH="src/main/resources/INPUT/Generated/R"
S_PATH=""
MIN_THRESHOLD=0.95
DISTANCE="J"
OUTPUT_HIST_LOCAL="src/main/resources/OUTPUT/TEST/HistMap"

HDFS_PORT="hdfs://localhost:9000/"
JAR_PATH="target/stagecleanV2-1.0-SNAPSHOT.jar"
INPUT="/input"
OUTPUT_HIST_DFS="/output_hist"

REDUCER_MEMORY=4096
FILLING_COEF=0.80
LIMIT=4
STEP=1

MAIN_CLASS="MainPrintHistMap"

## Prepare
#hdfs namenode -format
hadoop fs -rm -r $OUTPUT_HIST_DFS
hadoop fs -rm -r $INPUT
hadoop fs -mkdir $INPUT

if [[ $R_PATH != "" ]]; then
  hadoop fs -put $R_PATH $INPUT
fi

if [[ $S_PATH != "" ]]; then
  hadoop fs -put $S_PATH $INPUT
fi

hadoop jar $JAR_PATH $MAIN_CLASS $INPUT $OUTPUT_HIST_DFS $MIN_THRESHOLD $DISTANCE $HDFS_PORT $REDUCER_MEMORY $FILLING_COEF $LIMIT $STEP

if [[ $OUTPUT_HIST_LOCAL != "" ]]; then
  hadoop fs -getmerge $OUTPUT_HIST_DFS $OUTPUT_HIST_LOCAL
fi
