#!/bin/bash

R_PATH="src/main/resources/INPUT/Test/R"
S_PATH="src/main/resources/INPUT/Test/S"
MIN_THRESHOLD=0.90
DISTANCE="J"
OUTPUT_HIST_LOCAL=""
OUTPUT_JOIN_LOCAL="src/main/resources/OUTPUT/TEST/JoinMap"
JAR_PATH="target/stagecleanV2-1.0-SNAPSHOT.jar"

HDFS_PORT="hdfs://localhost:9000/"
INPUT="/input"
OUTPUT_HIST_DFS="/output_hist"
OUTPUT_JOIN_DFS="/output_join"

REDUCER_MEMORY=4096
FILLING_COEF=0.80
LIMIT=4
STEP=1

MAIN_CLASS="MainPrintJoinMap"

## Prepare
#hdfs namenode -format
hadoop fs -rm -r $OUTPUT_JOIN_DFS
hadoop fs -rm -r $OUTPUT_HIST_DFS
hadoop fs -rm -r $INPUT
hadoop fs -mkdir $INPUT

if [[ $R_PATH != "" ]]; then
  hadoop fs -put $R_PATH $INPUT
fi

if [[ $S_PATH != "" ]]; then
  hadoop fs -put $S_PATH $INPUT
fi

hadoop jar $JAR_PATH $MAIN_CLASS $INPUT $OUTPUT_HIST_DFS $OUTPUT_JOIN_DFS $MIN_THRESHOLD $DISTANCE $HDFS_PORT $REDUCER_MEMORY $FILLING_COEF $LIMIT $STEP

if [[ $OUTPUT_HIST_LOCAL != "" ]]; then
  hadoop fs -getmerge $OUTPUT_HIST_DFS $OUTPUT_HIST_LOCAL
fi

if [[ $OUTPUT_JOIN_LOCAL != "" ]]; then
  hadoop fs -getmerge $OUTPUT_JOIN_DFS  $OUTPUT_JOIN_LOCAL
fi