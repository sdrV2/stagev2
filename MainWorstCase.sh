#!/bin/bash

R_PATH="src/main/resources/INPUT/Generated/R"
S_PATH="src/main/resources/INPUT/Generated/S"
MIN_THRESHOLD=0.9
DISTANCE="J"
OUTPUT_JOIN_LOCAL="src/main/resources/OUTPUT/MainWorstCase/Gen/Join"
JAR_PATH="target/stagecleanV2-1.0-SNAPSHOT.jar"
HDFS_PORT="hdfs://localhost:9000/"
INPUT="/input"
OUTPUT_JOIN_DFS="/output_join"

MAIN_CLASS="MainWorstCase"

## Prepare
#hdfs namenode -format
hadoop fs -rm -r $OUTPUT_JOIN_DFS
hadoop fs -rm -r $INPUT
hadoop fs -mkdir $INPUT

if [[ $R_PATH != "" ]]; then
  hadoop fs -put $R_PATH $INPUT
fi

if [[ $S_PATH != "" ]]; then
  hadoop fs -put $S_PATH $INPUT
fi

hadoop jar $JAR_PATH $MAIN_CLASS $INPUT $OUTPUT_JOIN_DFS $MIN_THRESHOLD $DISTANCE $HDFS_PORT

if [[ $OUTPUT_JOIN_LOCAL != "" ]]; then
  hadoop fs -getmerge $OUTPUT_JOIN_DFS  $OUTPUT_JOIN_LOCAL
fi