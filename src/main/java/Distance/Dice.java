package Distance;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static Distance.ToolsDistance.getIntersectionList;
import static Tools.Tools.flattenSet;
import static Tools.Tools.getIndexes;

public class Dice {

    /*
    [0;1]
    0 if sets are totally different
    1 if sets are the same
     */

    public static Double DiceDist(HashMap<Character, List<String>> r, HashMap<Character, List<String>> s) {
        Set<String> intersection = new HashSet<>();

        for (Character keyR : r.keySet()) {
            //len_r += r.get(keyR).size();
            if (s.containsKey(keyR)) {
                for (String wordR : r.get(keyR)) {
                    for (String wordS : s.get(keyR)) {
                        if (wordS.equals(wordR)) {
                            intersection.add(wordR);
                        }
                    }
                }
            }
        }

        return Math.round(((double) (2 * intersection.size()) / (double) (flattenSet(r).size() + flattenSet(s).size())) * 100d) / 100d;
    }

    public static double[] DiceDist(HashMap<Character, List<String>> r, List<HashMap<Character, List<String>>> s, float minThreshold) {
        double[] res = new double[s.size()];
        int len_r = flattenSet(r).size();

        List<Set<String>> intersection = getIntersectionList(r, s);

        int[] Rindexes = diceIndexes(flattenSet(r).size(), minThreshold);

        for (int i = 0; i < s.size(); i++) {
            int len_s = flattenSet(s.get(i)).size();
            if (len_s >= Rindexes[0] && len_s <= Rindexes[1]) {
                res[i] = Math.round(((double) (2 * intersection.get(i).size()) / (double) (len_r + flattenSet(s.get(i)).size())) * 100d) / 100d;
            } else {
                res[i] = 9;
            }
        }

        return res;
    }

    public static int diceIndex(int size, float minThreshold) {
        return (int) Math.ceil((minThreshold * size) / (2 - minThreshold));
    }

    public static int[] diceIndexes(int size, float minThreshold) {
        return new int[]{(int) Math.ceil((minThreshold * size) / (2 - minThreshold)), (int) Math.floor(((2 * size) / minThreshold) - size)};
    }
}
