package Distance;

import java.util.*;

import static Distance.ToolsDistance.getIntersectionList;
import static Tools.Tools.flattenSet;
import static Tools.Tools.getIndexes;

public class Jaccard {

    /*
    [0;1]
    0 if sets are totally different
    1 if sets are the same
     */

    public static Double JaccardDist(HashMap<Character, List<String>> r, HashMap<Character, List<String>> s) {
        Set<String> intersectionSet = new HashSet<>();
        Set<String> unionSet = new HashSet<>();

        for (Character keyR : r.keySet()) {
            unionSet.addAll(r.get(keyR));
            if (s.containsKey(keyR)) {
                for (String wordR : r.get(keyR)) {
                    for (String wordS : s.get(keyR)) {
                        if (wordS.equals(wordR)) {
                            intersectionSet.add(wordR);
                        }
                    }
                }
            }
        }
        for (Character c : s.keySet()) {
            unionSet.addAll(s.get(c));
        }

        return Math.round(((double) intersectionSet.size() / (double) unionSet.size()) * 100d) / 100d;
    }

    public static double[] JaccardDist(HashMap<Character, List<String>> r, List<HashMap<Character, List<String>>> s, float minThreshold) {
        List<Set<String>> intersection;// = new ArrayList<>();
        List<Set<String>> union = new ArrayList<>();
        Set<String> rset = new HashSet<>(flattenSet(r));
        double[] res = new double[s.size()];

        for (int i = 0; i < s.size(); i++) {
            Set<String> set = new HashSet<>(flattenSet(s.get(i)));
            set.addAll(rset);
            union.add(set);
        }

        intersection = getIntersectionList(r, s);

        int[] Rindexes = jaccardIndexes(flattenSet(r).size(), minThreshold);

        for (int i = 0; i < s.size(); i++) {
            int len_s = flattenSet(s.get(i)).size();
            if (len_s >= Rindexes[0] && len_s <= Rindexes[1]) {
                res[i] = Math.round(((double) intersection.get(i).size() / (double) union.get(i).size()) * 100d) / 100d;
            } else {
                res[i] = 9;
            }
        }

        return res;
    }

    public static int jaccardIndex(int size, float minThreshold) {
        return (int) Math.ceil(size * minThreshold);
    }

    public static int[] jaccardIndexes(int size, float minThreshold) {
        return new int[]{(int) Math.ceil(size * minThreshold), (int) Math.floor(size / minThreshold)};
    }
}
