package Distance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static Distance.ToolsDistance.getIntersectionBisList;
import static Tools.Tools.flattenSet;
import static Tools.Tools.getIndexes;

public class JaccardFreq {

    /*
    [0;1]
    0 if sets are totally different
    1 if sets are the same

    "cou" & "coucou" -> 3/6 = 0.5
     */

    public static Double JaccardFreqDist(HashMap<Character, List<String>> r, HashMap<Character, List<String>> s) {
        List<String> intersection = new ArrayList<>();

        for (Character keyR : r.keySet()) {
            if (s.containsKey(keyR)) {
                for (String wordR : r.get(keyR)) {
                    if (!intersection.contains(wordR)) {
                        for (int i = 0; i < Math.min(r.get(keyR).stream().filter(p -> p.equals(wordR)).count(), s.get(keyR).stream().filter(p -> p.equals(wordR)).count()); i++) {
                            intersection.add(wordR);
                        }
                    }
                }
            }
        }

        return Math.round(((double) intersection.size() / (double) (flattenSet(r).size() + flattenSet(s).size() - intersection.size())) * 100d) / 100d;
    }

    public static double[] JaccardFreqDist(HashMap<Character, List<String>> r, List<HashMap<Character, List<String>>> s, float minThreshold) {
        double[] res = new double[s.size()];
        int len_r = flattenSet(r).size();

        List<List<String>> intersection = getIntersectionBisList(r, s);

        int[] Rindexes = jaccardFreqIndexes(flattenSet(r).size(), minThreshold);

        for (int i = 0; i < s.size(); i++) {
            int len_s = flattenSet(s.get(i)).size();
            if (len_s >= Rindexes[0] && len_s <= Rindexes[1]) {
                res[i] = Math.round(((double) intersection.get(i).size() / (double) (len_r + flattenSet(s.get(i)).size() - intersection.get(i).size())) * 100d) / 100d;
            } else {
                res[i] = 9;
            }
        }

        return res;
    }

    public static int jaccardFreqIndex(int size, float minThreshold) {
        return (int) Math.ceil(size * minThreshold);
    }

    public static int[] jaccardFreqIndexes(int size, float minThreshold) {
        return new int[]{(int) Math.ceil(size * minThreshold), (int) Math.floor(size / minThreshold)};
    }
}
