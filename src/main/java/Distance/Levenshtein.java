package Distance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static Distance.ToolsDistance.getIntersectionBisList;
import static Tools.Tools.flattenSet;
import static Tools.Tools.getIndexes;

public class Levenshtein {


    public static Double LevenshteinDist(HashMap<Character, List<String>> r, HashMap<Character, List<String>> s) {
        int len_r = flattenSet(r).size();
        int len_s = flattenSet(s).size();

        if (len_r > len_s) {
            double res = (len_s - dist(r, s) + len_r - len_s);
            if(res==0){
                return res;
            }else {
                return Math.round((res / len_s) * 100d) / 100d;
            }
        } else {
            double res = (len_r - dist(s, r) + len_s - len_r);
            if(res==0){
                return res;
            }else {
                return Math.round((res / len_r) * 100d) / 100d;
            }
        }
    }

    private static float dist(HashMap<Character, List<String>> r, HashMap<Character, List<String>> s) {
        List<String> intersection = new ArrayList<>();

        for (Character keyS : s.keySet()) {
            if (r.containsKey(keyS)) {
                for (String wordS : s.get(keyS)) {
                    if (!intersection.contains(wordS)) {
                        for (int i = 0; i < Math.min(r.get(keyS).stream().filter(p -> p.equals(wordS)).count(), s.get(keyS).stream().filter(p -> p.equals(wordS)).count()); i++) {
                            intersection.add(wordS);
                        }
                    }
                }
            }
        }

        return (float) intersection.size();
    }

    public static double[] LevenshteinDist(HashMap<Character, List<String>> r, List<HashMap<Character, List<String>>> s, float minThreshold) {
        double[] res = new double[s.size()];
        int len_r = flattenSet(r).size();

        List<List<String>> intersection = getIntersectionBisList(r, s);

        int[] Rindexes = levenshteinIndexes(flattenSet(r).size(), minThreshold);

        for (int i = 0; i < s.size(); i++) {
            int len_s = flattenSet(s.get(i)).size();
            if (len_s >= Rindexes[0] && len_s <= Rindexes[1]) {
                if (len_r > len_s) {
                    double tmp = len_s - intersection.get(i).size() + len_r - len_s;
                    if(tmp==0){
                        res[i]=tmp;
                    }else {
                        res[i] = Math.round(((float) tmp / len_s) * 100d) / 100d;
                    }
                } else {
                    double tmp = len_r - intersection.get(i).size() + len_s - len_r;
                    if(tmp==0){
                        res[i]=tmp;
                    }else {
                        res[i] = Math.round(((float) tmp / len_r) * 100d) / 100d;
                    }
                }
            } else {
                res[i] = 9;
            }
        }

        return res;
    }

    public static int levenshteinIndex(int size, float minThreshold) {
        return (int) (size - minThreshold);
    }

    public static int[] levenshteinIndexes(int size, float minThreshold) {
        return new int[]{(int) (size - minThreshold), (int) (size + minThreshold)};
    }
}
