package Distance;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static Distance.ToolsDistance.getIntersectionList;
import static Tools.Tools.flattenSet;

public class Overlap {

    /*
    [0;1]
    0 if sets are totally different
    1 if sets are the same
     */

    public static Double OverlapDist(HashMap<Character, List<String>> r, HashMap<Character, List<String>> s) {
        Set<String> intersection = new HashSet<>();
        int len_r = 0;
        int len_s = 0;

        for (Character keyR : r.keySet()) {
            len_r += r.get(keyR).size();
            if (s.containsKey(keyR)) {
                for (String wordR : r.get(keyR)) {
                    for (String wordS : s.get(keyR)) {
                        if (wordS.equals(wordR)) {
                            intersection.add(wordR);
                        }
                    }
                }
            }
        }

        for (Character keyS : s.keySet()) {
            len_s += s.get(keyS).size();
        }

        return Math.round(((double) intersection.size() / (double) Math.min(len_r, len_s)) * 100d) / 100d;
    }

    public static double[] OverlapDist(HashMap<Character, List<String>> r, List<HashMap<Character, List<String>>> s) {
        double[] res = new double[s.size()];
        int len_r = flattenSet(r).size();

        List<Set<String>> intersection = getIntersectionList(r, s);

        for (int i = 0; i < s.size(); i++) {
            res[i] = Math.round(((double) intersection.get(i).size() / Math.min(len_r, flattenSet(s.get(i)).size())) * 100d) / 100d;
        }

        return res;
    }

    public static int overlapIndex(int size, float minThreshold) {
        return 1;
    }

    public static int[] overlapIndexes(int size, float minThreshold) {
        return new int[]{1, 999999999};
    }
}
