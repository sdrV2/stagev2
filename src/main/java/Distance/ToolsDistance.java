package Distance;

import java.util.*;

public class ToolsDistance {

    public static List<Set<String>> getIntersectionList(HashMap<Character, List<String>> r, List<HashMap<Character, List<String>>> s) {
        List<Set<String>> intersection = new ArrayList<>();
        for (int i = 0; i < s.size(); i++) {
            intersection.add(new HashSet<>());
        }

        for (Character keyR : r.keySet()) {
            for (int i = 0; i < s.size(); i++) {
                if (s.get(i).containsKey(keyR)) {
                    for (String wordR : r.get(keyR)) {
                        for (String wordS : s.get(i).get(keyR)) {
                            if (wordS.equals(wordR)) {
                                intersection.get(i).add(wordR);
                            }
                        }
                    }
                }
            }
        }
        return intersection;
    }

    public static List<List<String>> getIntersectionBisList(HashMap<Character, List<String>> r, List<HashMap<Character, List<String>>> s) {
        List<List<String>> intersection = new ArrayList<>();
        for (int i = 0; i < s.size(); i++) {
            intersection.add(new ArrayList<>());
        }

        for (Character keyR : r.keySet()) {
            for (int i = 0; i < s.size(); i++) {
                if (s.get(i).containsKey(keyR)) {
                    for (String wordR : r.get(keyR)) {
                        if (!intersection.get(i).contains(wordR)) {
                            for (int j = 0; j < Math.min(r.get(keyR).stream().filter(p -> p.equals(wordR)).count(), s.get(i).get(keyR).stream().filter(p -> p.equals(wordR)).count()); j++) {
                                intersection.get(i).add(wordR);
                            }
                        }
                    }
                }
            }
        }
        return intersection;
    }

    public static List<Set<String>> getUnionAndIntersection(HashMap<Character, List<String>> r, List<HashMap<Character, List<String>>> s) {
        List<Set<String>> intersection = new ArrayList<>();
        List<Set<String>> union = new ArrayList<>();
        for (int i = 0; i < s.size(); i++) {
            intersection.add(new HashSet<>());
            union.add(new HashSet<>());
        }

        for (Character keyR : r.keySet()) {
            //union.get().addAll(r.get(keyR));
            for (int i = 0; i < s.size(); i++) {
                if (s.get(i).containsKey(keyR)) {
                    for (String wordR : r.get(keyR)) {
                        for (String wordS : s.get(i).get(keyR)) {
                            if (wordS.equals(wordR)) {
                                intersection.get(i).add(wordR);
                            }
                        }
                    }
                }
            }
        }
        return intersection;
    }
}
