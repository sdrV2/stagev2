package InOut;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class JoinKey implements WritableComparable<JoinKey> {
    private LongWritable globalSize;
    private LongWritable bucketKey;

    public JoinKey() {
    }

    public JoinKey(LongWritable globalSize, LongWritable bucketKey) {
        this.globalSize = globalSize;
        this.bucketKey = bucketKey;
    }

    @Override
    public String toString() {
        return "JoinKey{" +
                "globalSize=" + globalSize +
                ", bucketKey=" + bucketKey +
                '}';
    }

    public LongWritable getGlobalSize() {
        return globalSize;
    }

    public void setGlobalSize(LongWritable globalSize) {
        this.globalSize = globalSize;
    }

    public LongWritable getBucketKey() {
        return bucketKey;
    }

    public void setBucketKey(LongWritable bucketKey) {
        this.bucketKey = bucketKey;
    }

    @Override
    public int compareTo(JoinKey o) {
        if (globalSize.compareTo(o.globalSize) == 0) {
            return bucketKey.compareTo(o.bucketKey);
        } else {
            return globalSize.compareTo(o.globalSize);
        }
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        globalSize.write(dataOutput);
        bucketKey.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        globalSize = new LongWritable(dataInput.readLong());
        bucketKey = new LongWritable(dataInput.readLong());
    }
}
