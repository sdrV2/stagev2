package InOut;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjA implements WritableComparable<ObjA> {
    private IntWritable size;
    private List<IntWritable> memory;
    private Map<Text, IntWritable> map;

    public ObjA() {
    }

    public ObjA(IntWritable size, List<IntWritable> memory, Map<Text, IntWritable> map) {
        this.size = size;
        this.memory = memory;
        this.map = map;
    }

    public IntWritable getSize() {
        return size;
    }

    public void setSize(IntWritable size) {
        this.size = size;
    }

    public List<IntWritable> getMemory() {
        return memory;
    }

    public void setMemory(List<IntWritable> memory) {
        this.memory = memory;
    }

    public Map<Text, IntWritable> getMap() {
        return map;
    }

    public void setMap(Map<Text, IntWritable> map) {
        this.map = map;
    }

    public void clear() {
        size = new IntWritable();
        memory = new ArrayList<>();
        map = new HashMap<>();
    }

    @Override
    public String toString() {
        String print = "";
        for (Text k : map.keySet()) {
            print = print.concat(k + " = " + map.get(k));
        }

        return "ObjA{" +
                "size=" + size +
                ", memory=(" + memory.toString() +
                ", map=(" + print +
                ")}";
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        size.write(dataOutput);

        dataOutput.writeInt(memory.size());
        for (IntWritable aByte : memory) {
            dataOutput.writeInt(aByte.get());
        }

        dataOutput.writeInt(map.size());
        for (Text k : map.keySet()) {
            k.write(dataOutput);
            map.get(k).write(dataOutput);
        }
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.clear();
        size.readFields(dataInput);

        int len = dataInput.readInt();
        for (int i = 0; i < len; i++) {
            IntWritable v = new IntWritable(dataInput.readInt());
            memory.add(v);
        }

        len = dataInput.readInt();
        for (int i = 0; i < len; i++) {
            Text k = new Text(Text.readString(dataInput));
            IntWritable v = new IntWritable(dataInput.readInt());
            map.put(k, v);
        }

    }

    @Override
    public int compareTo(ObjA o) {
        return size.get() - o.size.get();
    }
}
