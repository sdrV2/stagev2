package InOut;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ObjB implements WritableComparable<ObjB> {
    private HashMap<Character, List<String>> set;
    private Text file;
    private IntWritable setSize;


    public ObjB() {
    }

    public ObjB(HashMap<Character, List<String>> set, Text file, IntWritable globalSize) {
        this.set = set;
        this.file = file;
        this.setSize = globalSize;
    }

    public ObjB(ObjB objB) {
        this.file = new Text(objB.getFile());
        this.setSize = new IntWritable(objB.getSetSize().get());
        this.set = new HashMap<>(objB.getSet());
    }

    @Override
    public String toString() {
        return "ObjB{" +
                "set=" + set.toString() +
                ", file=" + file +
                ", setSize=" + setSize +
                '}';
    }

    public HashMap<Character, List<String>> getSet() {
        return set;
    }

    public void setSet(HashMap<Character, List<String>> set) {
        this.set = set;
    }

    public Text getFile() {
        return file;
    }

    public void setFile(Text file) {
        this.file = file;
    }

    public IntWritable getSetSize() {
        return setSize;
    }

    public void setSetSize(IntWritable setSize) {
        this.setSize = setSize;
    }

    public ArrayList<String> flattenSet() {
        ArrayList<String> res = new ArrayList<>();
        for (Character c : set.keySet()) {
            res.addAll(set.get(c));
        }
        return res;
    }

    public void clear() {
        set = new HashMap<>();
        file = new Text();
        setSize = new IntWritable();
    }

    @Override
    public int compareTo(ObjB o) {
        return 0;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        file.write(dataOutput);
        setSize.write(dataOutput);
        dataOutput.writeInt(set.size());
        for (char c : set.keySet()) {
            dataOutput.writeInt(set.get(c).size());
            for (String s : set.get(c)) {
                new Text(s).write(dataOutput);
            }
        }
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.clear();
        file.readFields(dataInput);
        setSize.readFields(dataInput);
        int len = dataInput.readInt();
        List<String> list = new ArrayList<>();

        for (int i = 0; i < len; i++) {
            int y = dataInput.readInt();
            list.clear();
            String s = null;
            for (int j = 0; j < y; j++) {
                s = new Text(Text.readString(dataInput)).toString();
                list.add(s);
            }
            assert s != null;
            set.put(new StringBuilder(s).charAt(0), new ArrayList<>(list));
        }
    }
}
