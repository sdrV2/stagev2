package InOut;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;

public class ObjC implements WritableComparable<ObjC> {
    private Text r_file;
    private Text s_file;
    private ArrayList<String> r;
    private ArrayList<String> s;


    public ObjC() {
    }

    public ObjC(Text r_file, Text s_file, ArrayList<String> r, ArrayList<String> s) {
        this.r_file = r_file;
        this.s_file = s_file;
        this.r = r;
        this.s = s;
    }

    @Override
    public String toString() {
        return r_file + " = " + r + ",  " + s_file + " = " + s;
    }

    public Text getR_file() {
        return r_file;
    }

    public void setR_file(Text r_file) {
        this.r_file = r_file;
    }

    public Text getS_file() {
        return s_file;
    }

    public void setS_file(Text s_file) {
        this.s_file = s_file;
    }

    public ArrayList<String> getR() {
        return r;
    }

    public void setR(ArrayList<String> r) {
        this.r = r;
    }

    public ArrayList<String> getS() {
        return s;
    }

    public void setS(ArrayList<String> s) {
        this.s = s;
    }


    @Override
    public int compareTo(ObjC o) {
        return 0;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        r_file.write(dataOutput);
        s_file.write(dataOutput);
        dataOutput.writeInt(r.size());
        for (String mot : r) {
            Text t = new Text(mot);
            t.write(dataOutput);
        }
        dataOutput.writeInt(s.size());
        for (String mot : s) {
            Text t = new Text(mot);
            t.write(dataOutput);
        }
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        r_file.readFields(dataInput);
        s_file.readFields(dataInput);
        r.clear();
        int len_r = dataInput.readInt();
        for (int i = 0; i < len_r; i++) {
            Text t = new Text(Text.readString(dataInput));
            r.add(t.toString());
        }
        s.clear();
        int len_s = dataInput.readInt();
        for (int i = 0; i < len_s; i++) {
            Text t = new Text(Text.readString(dataInput));
            s.add(t.toString());
        }
    }
}
