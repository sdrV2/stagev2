package InOut;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ObjD implements WritableComparable<ObjD> {
    private Map<Text, LongWritable> map;

    public ObjD() {
    }

    public ObjD(Map<Text, LongWritable> map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return ";" + map.toString();
    }

    public Map<Text, LongWritable> getMap() {
        return map;
    }

    public void setMap(Map<Text, LongWritable> map) {
        this.map = map;
    }

    @Override
    public int compareTo(ObjD objD) {
        if (map.equals(objD.getMap())) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(map.size());
        for (Text k : map.keySet()) {
            k.write(dataOutput);
            map.get(k).write(dataOutput);
        }
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        map = new HashMap<>();
        int len = dataInput.readInt();
        for (int i = 0; i < len; i++) {
            Text k = new Text(Text.readString(dataInput));
            LongWritable v = new LongWritable(dataInput.readLong());
            map.put(k, v);
        }
    }
}
