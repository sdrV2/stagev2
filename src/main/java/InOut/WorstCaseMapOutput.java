package InOut;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorstCaseMapOutput implements WritableComparable<WorstCaseMapOutput> {
    private Text fileName;
    private HashMap<Character, List<String>> set;

    public WorstCaseMapOutput() {
    }

    public WorstCaseMapOutput(Text fileName, HashMap<Character, List<String>> set) {
        this.fileName = fileName;
        this.set = set;
    }

    public WorstCaseMapOutput(WorstCaseMapOutput val) {
        this.fileName = val.getFileName();
        this.set = val.getSet();
    }

    public Text getFileName() {
        return fileName;
    }

    public void setFileName(Text fileName) {
        this.fileName = fileName;
    }

    public HashMap<Character, List<String>> getSet() {
        return set;
    }

    public void setSet(HashMap<Character, List<String>> set) {
        this.set = set;
    }

    public ArrayList<String> flattenSet() {
        ArrayList<String> res = new ArrayList<>();
        for (Character c : set.keySet()) {
            res.addAll(set.get(c));
        }
        return res;
    }

    public void clear() {
        set = new HashMap<>();
        fileName = new Text();
    }

    @Override
    public int compareTo(WorstCaseMapOutput worstCaseMapOutput) {
        return 0;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        fileName.write(dataOutput);
        dataOutput.writeInt(set.size());
        for (char c : set.keySet()) {
            dataOutput.writeInt(set.get(c).size());
            for (String s : set.get(c)) {
                new Text(s).write(dataOutput);
            }
        }
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.clear();
        fileName.readFields(dataInput);
        int len = dataInput.readInt();
        List<String> list = new ArrayList<>();

        for (int i = 0; i < len; i++) {
            int y = dataInput.readInt();
            list.clear();
            String s = null;
            for (int j = 0; j < y; j++) {
                s = new Text(Text.readString(dataInput)).toString();
                list.add(s);
            }
            assert s != null;
            set.put(new StringBuilder(s).charAt(0), new ArrayList<>(list));
        }
    }
}
