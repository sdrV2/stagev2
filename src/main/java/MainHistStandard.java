import InOut.ObjD;
import MapRed.Hist;
import MapRed.HistogramStandard;
import MapRed.Join;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;

/*
 : $JAR_PATH
 : $MAIN_CLASS
0 : $INPUT
1 : $OUTPUT_HIST_DFS
2 : $HDFS_PORT
 */

public class MainHistStandard {
    public static String hdfs_port;

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        long start = System.currentTimeMillis();

        try {
            hdfs_port = args[2];
        } catch (Exception e) {
            System.err.println(e.toString());
            System.exit(1);
        }

        Configuration confHist = new Configuration();
        String[] otherArgs = new GenericOptionsParser(confHist, args).getRemainingArgs();
        Job jobHist = Job.getInstance(confHist);
        jobHist.setJarByClass(HistogramStandard.class);
        jobHist.setMapperClass(HistogramStandard.Map.class);
        jobHist.setCombinerClass(HistogramStandard.Reduce.class);
        jobHist.setPartitionerClass(HistogramStandard.Partitioner.class);
        jobHist.setReducerClass(HistogramStandard.Reduce.class);
        jobHist.setOutputKeyClass(LongWritable.class);
        jobHist.setOutputValueClass(ObjD.class);
        jobHist.setMapOutputKeyClass(LongWritable.class);
        jobHist.setMapOutputValueClass(ObjD.class);
        FileInputFormat.addInputPath(jobHist, new Path(otherArgs[0]));
        FileOutputFormat.setOutputPath(jobHist, new Path(otherArgs[1]));

        try {
            int count = 0;
            FileSystem fs = FileSystem.get(confHist);
            RemoteIterator<LocatedFileStatus> ri = fs.listFiles(new Path(hdfs_port + args[0]), false);
            while (ri.hasNext()) {
                count++;
                ri.next();
            }
            if (count == 1) {
                Join.selfJoin = true;
            } else if (count == 2) {
                Join.selfJoin = false;
            } else {
                System.exit(3);
            }
            fs.close();
        } catch (Exception e) {
            System.err.println(e.toString());
            System.exit(4);
        }

        jobHist.waitForCompletion(true);
        long stop = System.currentTimeMillis();
        System.out.println("TIME TOTAL = " + (stop - start));

        System.exit(0);
    }
}
