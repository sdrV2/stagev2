import InOut.ObjA;
import MapRed.Hist;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;

import static Tools.Tools.checkDist;

/*
 : $JAR_PATH
 : $MAIN_CLASS
0 : $INPUT
1 : $OUTPUT_HIST_DFS
2 : $MIN_THRESHOLD
3 : $DISTANCE
4 : $HDFS_PORT
5 : $REDUCER_MEMORY
6 : $FILLING_COEF
7 : LIMIT
8 : STEP
 */

public class MainPrintHistMap {
    public static String hdfs_port;

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        long start = System.currentTimeMillis();

        try {
            if (!checkDist(args[3], Float.parseFloat(args[2]))) {
                System.exit(1);
            }
            Hist.minThreshold = Float.parseFloat(args[2]);
            Hist.dist = args[3];
            hdfs_port = args[4];
            Hist.step = Integer.parseInt(args[8]);
            Hist.useLimit = Long.parseLong(args[7]) != 0;
        } catch (Exception e) {
            System.err.println(e.toString());
            System.exit(1);
        }

        Configuration confHist = new Configuration();
        String[] otherArgs = new GenericOptionsParser(confHist, args).getRemainingArgs();
        Job jobHist = Job.getInstance(confHist);
        jobHist.setJarByClass(Hist.class);
        jobHist.setMapperClass(Hist.Map.class);
        jobHist.setPartitionerClass(Hist.Partitioner.class);
        jobHist.setReducerClass(Hist.PrintMap.class);
        jobHist.setOutputKeyClass(LongWritable.class);
        jobHist.setOutputValueClass(ObjA.class);
        jobHist.setMapOutputKeyClass(LongWritable.class);
        jobHist.setMapOutputValueClass(ObjA.class);
        FileInputFormat.addInputPath(jobHist, new Path(otherArgs[0]));
        FileOutputFormat.setOutputPath(jobHist, new Path(otherArgs[1]));

        try {
            int count = 0;
            FileSystem fs = FileSystem.get(confHist);
            RemoteIterator<LocatedFileStatus> ri = fs.listFiles(new Path(hdfs_port + args[0]), false);
            while (ri.hasNext()) {
                count++;
                ri.next();
            }
            if (count == 1) {
                Hist.selfJoin = true;
            } else if (count == 2) {
                Hist.selfJoin = false;
            } else {
                System.exit(3);
            }
            fs.close();
        } catch (Exception e) {
            System.err.println(e.toString());
            System.exit(4);
        }

        jobHist.waitForCompletion(true);
        long stop = System.currentTimeMillis();
        System.out.println("TIME TOTAL = " + (stop - start));

        System.exit(0);
    }
}
