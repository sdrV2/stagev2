import InOut.JoinKey;
import InOut.ObjA;
import InOut.ObjB;
import MapRed.Hist;
import MapRed.Join;
import Tools.CacheData;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static Tools.Tools.checkDist;

    /*
 : $JAR_PATH
 : $MAIN_CLASS
0 : $INPUT
1 : $OUTPUT_HIST_DFS
2 : $OUTPUT_JOIN_DFS
3 : $MIN_THRESHOLD
4 : $DISTANCE
5 : $HDFS_PORT
6 : $REDUCER_MEMORY
7 : $FILLING_COEF
8 : LIMIT
9 : STEP
 */

public class MainPrintJoinMap {
    public static String hdfs_port;

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException, URISyntaxException {
        long start = System.currentTimeMillis();

        try {
            if (!checkDist(args[4], Float.parseFloat(args[3]))) {
                System.exit(1);
            }
            Hist.minThreshold = Float.parseFloat(args[3]);
            Join.minThreshold = Float.parseFloat(args[3]);
            Hist.dist = args[4];
            Join.dist = args[4];
            hdfs_port = args[5];
            Join.hdfs_port = args[5];
            Hist.step = Integer.parseInt(args[9]);
            Join.step = Integer.parseInt(args[9]);
            if (Long.parseLong(args[9]) != 0) {
                Hist.useLimit = true;
                Join.limitByReducer = Long.parseLong(args[8]);
                Join.useLimit = true;
            } else {
                Hist.useLimit = false;
                Join.useLimit = false;
                Join.reducer_memory = Long.parseLong(args[6]) * 1000000;
                Join.filling_coef = Float.parseFloat(args[7]);
            }
        } catch (Exception e) {
            System.err.println(e.toString());
            System.out.println(e.toString());
            System.exit(1);
        }

        Configuration confHist = new Configuration();
        String[] otherArgs = new GenericOptionsParser(confHist, args).getRemainingArgs();
        Job jobHist = Job.getInstance(confHist);
        jobHist.setJarByClass(Hist.class);
        jobHist.setMapperClass(Hist.Map.class);
        jobHist.setCombinerClass(Hist.Combiner.class);
        jobHist.setPartitionerClass(Hist.Partitioner.class);
        jobHist.setReducerClass(Hist.Reduce.class);
        jobHist.setOutputKeyClass(LongWritable.class);
        jobHist.setOutputValueClass(CacheData.class);
        jobHist.setMapOutputKeyClass(LongWritable.class);
        jobHist.setMapOutputValueClass(ObjA.class);
        FileInputFormat.addInputPath(jobHist, new Path(otherArgs[0]));
        FileOutputFormat.setOutputPath(jobHist, new Path(otherArgs[1]));

        try {
            int count = 0;
            FileSystem fs = FileSystem.get(confHist);
            RemoteIterator<LocatedFileStatus> ri = fs.listFiles(new Path(hdfs_port + args[0]), false);
            while (ri.hasNext()) {
                count++;
                ri.next();
            }
            if (count == 1) {
                Join.selfJoin = true;
                Hist.selfJoin = true;
            } else if (count == 2) {
                Join.selfJoin = false;
                Hist.selfJoin = false;
            } else {
                System.exit(3);
            }
            fs.close();
        } catch (Exception e) {
            System.err.println(e.toString());
            System.out.println("33\n");
            System.exit(4);
        }

        jobHist.waitForCompletion(true);
        long stop = System.currentTimeMillis();
        long start_join = System.currentTimeMillis();

        Configuration confJoin = new Configuration();
        Job jobJoin = Job.getInstance(confJoin);

        FileSystem fs = FileSystem.get(confJoin);
        RemoteIterator<LocatedFileStatus> fileStatusListIterator = fs.listFiles(new Path(args[1]), true);
        StringBuilder s = new StringBuilder(args[1]);
        s.deleteCharAt(0);
        while (fileStatusListIterator.hasNext()) {
            LocatedFileStatus fileStatus = fileStatusListIterator.next();
            if (fileStatus.getPath().toString().startsWith(hdfs_port + s.toString() + "/part")) {
                jobJoin.addCacheFile(new URI(fileStatus.getPath().toString()));
            }
        }

        jobJoin.setJarByClass(Join.class);
        jobJoin.setMapperClass(Join.Map.class);
        jobJoin.setPartitionerClass(Join.Partitioner.class);
        jobJoin.setReducerClass(Join.PrintMap.class);
        jobJoin.setOutputKeyClass(JoinKey.class);
        jobJoin.setOutputValueClass(ObjB.class);
        jobJoin.setMapOutputKeyClass(JoinKey.class);
        jobJoin.setMapOutputValueClass(ObjB.class);
        FileInputFormat.addInputPath(jobJoin, new Path(otherArgs[0]));
        FileOutputFormat.setOutputPath(jobJoin, new Path(otherArgs[2]));
        jobJoin.waitForCompletion(true);
        System.out.println("TIME HIST = " + (stop - start));
        stop = System.currentTimeMillis();
        System.out.println("TIME JOIN = " + (stop - start_join));
        System.out.println("TIME TOTAL = " + (stop - start));
        System.exit(0);
    }
}
