import InOut.ObjB;
import InOut.ObjC;
import InOut.WorstCaseMapOutput;
import MapRed.Hist;
import MapRed.Join;
import MapRed.WorstCase;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

import static Tools.Tools.checkDist;

 /*
 : $JAR_PATH
 : $MAIN_CLASS
0 : $INPUT
1 : $OUTPUT_JOIN_DFS
2 : $MIN_THRESHOLD
3 : $DISTANCE
4 : $HDFS_PORT

 */

public class MainWorstCase {
    public static String hdfs_port;

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {

        try {
            if (!checkDist(args[3], Float.parseFloat(args[2]))) {
                System.exit(1);
            }
            WorstCase.minThreshold = Float.parseFloat(args[2]);
            WorstCase.dist = args[3];
            hdfs_port = args[4];
        } catch (Exception e) {
            System.err.println(e.toString());
            System.exit(2);
        }

        long start = System.currentTimeMillis();

        Configuration confJoin = new Configuration();
        Job jobJoin = Job.getInstance(confJoin);
        jobJoin.setJarByClass(WorstCase.class);
        jobJoin.setMapperClass(WorstCase.Map.class);
        jobJoin.setReducerClass(WorstCase.Reduce.class);
        jobJoin.setOutputKeyClass(DoubleWritable.class);
        jobJoin.setOutputValueClass(ObjC.class);
        jobJoin.setMapOutputKeyClass(IntWritable.class);
        jobJoin.setMapOutputValueClass(WorstCaseMapOutput.class);
        FileInputFormat.addInputPath(jobJoin, new Path(args[0]));
        FileOutputFormat.setOutputPath(jobJoin, new Path(args[1]));

        try {
            int count = 0;
            FileSystem fs = FileSystem.get(confJoin);
            RemoteIterator<LocatedFileStatus> ri = fs.listFiles(new Path(hdfs_port + args[0]), false);
            while (ri.hasNext()) {
                count++;
                ri.next();
            }
            if (count == 1) {
                WorstCase.selfJoin = true;
            } else if (count == 2) {
                WorstCase.selfJoin = false;
            } else {
                System.exit(3);
            }
            fs.close();
        } catch (Exception e) {
            System.err.println(e.toString());
            System.exit(4);
        }

        jobJoin.waitForCompletion(true);
        long stop = System.currentTimeMillis();
        System.out.println("TIME TOTAL = " + (stop - start));
        System.exit(0);
    }
}
