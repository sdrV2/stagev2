package MapRed;

import InOut.ObjA;
import Tools.CacheData;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.TreeMap;

import static Tools.Tools.*;


public class Hist {
    public static float minThreshold;
    public static boolean selfJoin;
    public static String dist;
    public static boolean useLimit;
    public static int step;


    public static class Map extends Mapper<Object, Text, LongWritable, ObjA> {
        private static final IntWritable one = new IntWritable(1);
        private final java.util.Map<Text, IntWritable> map = new HashMap<>();
        private final ArrayList<IntWritable> list = new ArrayList<>();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString());
            map.clear();
            list.clear();
            int size = 0;
            Text fileName = new Text(((FileSplit) context.getInputSplit()).getPath().getName());
            int min_elem = 999999999;

            while (itr.hasMoreTokens()) {
                String elem = itr.nextToken();
                if (elem.length() < min_elem) {
                    min_elem = elem.length();
                }
                size++;
            }

            context.write(new LongWritable(-1), new ObjA(new IntWritable(min_elem), new ArrayList<>(), new HashMap<>()));
            map.put(fileName, one);

            if (!useLimit) {
                list.add(new IntWritable(estimateMemory(value.toString(), size)));
            }

            for (int i = getIndex(dist, size, minThreshold); i <= getMax(size, step); i++) {
                if (i % step == 0) {
                    context.write(new LongWritable(i), new ObjA(new IntWritable(size), list, map));
                }
            }
        }
    }

    public static class Partitioner extends org.apache.hadoop.mapreduce.Partitioner<LongWritable, ObjA> {

        @Override
        public int getPartition(LongWritable size, ObjA val, int i) {
            return (int) (size.get() % i);
        }
    }

    public static class Combiner extends Reducer<LongWritable, ObjA, LongWritable, ObjA> {
        private final java.util.Map<Text, IntWritable> mapR = new HashMap<>();
        private final java.util.Map<Text, IntWritable> mapS = new HashMap<>();
        private final ArrayList<IntWritable> listR = new ArrayList<>();
        private final ArrayList<IntWritable> listS = new ArrayList<>();
        private Text RFile = null;

        public void reduce(LongWritable size, Iterable<ObjA> values, Context context) throws IOException, InterruptedException {
            mapR.clear();
            mapS.clear();
            listR.clear();
            listS.clear();
            int maxR = 999999999;
            int maxS = 999999999;

            if (size.get() != -1) {
                for (ObjA val : values) {
                    for (Text k : val.getMap().keySet()) {
                        if (RFile == null) {
                            RFile = k;
                        }
                        IntWritable count = val.getMap().get(k);
                        int len = val.getSize().get();
                        if (k.equals(RFile)) {
                            if (len < maxR) {
                                maxR = len;
                            }
                            if (mapR.containsKey(k)) {
                                mapR.put(k, new IntWritable(mapR.get(k).get() + count.get()));
                            } else {
                                mapR.put(k, count);
                            }
                            if (!useLimit) {
                                listR.addAll(val.getMemory());
                            }
                        } else {
                            if (len < maxS) {
                                maxS = len;
                            }
                            if (mapS.containsKey(k)) {
                                mapS.put(k, new IntWritable(mapS.get(k).get() + count.get()));
                            } else {
                                mapS.put(k, count);
                            }
                            if (!useLimit) {
                                listS.addAll(val.getMemory());
                            }
                        }
                    }
                }
                if (maxR < 999999999) {
                    context.write(size, new ObjA(new IntWritable(maxR), listR, mapR));
                }
                if (maxS < 999999999) {
                    context.write(size, new ObjA(new IntWritable(maxS), listS, mapS));
                }
            } else {
                int min_elem = 999999999;
                for (ObjA val : values) {
                    if (val.getSize().get() < min_elem) {
                        min_elem = val.getSize().get();
                    }
                }
                context.write(new LongWritable(-1), new ObjA(new IntWritable(min_elem), new ArrayList<>(), new HashMap<>()));
            }
        }
    }

    public static class Reduce extends Reducer<LongWritable, ObjA, LongWritable, CacheData> {
        private final java.util.Map<Text, Integer> map = new TreeMap<>();

        public void reduce(LongWritable size, Iterable<ObjA> values, Context context) throws IOException, InterruptedException {
            map.clear();
            boolean ok = false;
            float memory = 0;
            int memory_count = 0;

            if (size.get() != -1) {
                for (ObjA val : values) {
                    if (!ok) {
                        if (val.getSize().get() <= size.get()) {
                            ok = true;
                        }
                    }
                    for (Text k : val.getMap().keySet()) {
                        if (map.containsKey(k)) {
                            int x = map.get(k);
                            map.put(new Text(k.toString()), (x + val.getMap().get(k).get()));
                        } else {
                            map.put(k, val.getMap().get(k).get());
                        }
                    }

                    if (!useLimit) {
                        for (IntWritable mem : val.getMemory()) {
                            memory = (memory * memory_count + mem.get()) / (memory_count + 1);
                            memory_count++;
                        }
                    }
                }

                if (ok) {
                    context.write(size, new CacheData(new IntWritable((int) Math.ceil(memory)), new TreeMap<>(map)));
                }
            } else {
                int min_elem = 999999999;
                for (ObjA val : values) {
                    if (val.getSize().get() < min_elem) {
                        min_elem = val.getSize().get();
                    }
                }
                context.write(new LongWritable(-1), new CacheData(new IntWritable(min_elem), new TreeMap<>()));
            }
        }
    }

    public static class PrintMap extends Reducer<LongWritable, ObjA, LongWritable, ObjA> {

        public void reduce(LongWritable size, Iterable<ObjA> values, Context context) throws IOException, InterruptedException {
            for (ObjA val : values) {
                context.write(size, val);
            }
        }
    }

}
