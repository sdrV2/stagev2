package MapRed;

import InOut.ObjD;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;

public class HistogramStandard {

    public static class Map extends Mapper<Object, Text, LongWritable, ObjD> {
        private static final LongWritable one = new LongWritable(1);
        private final java.util.Map<Text, LongWritable> map = new HashMap<>();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString());
            map.clear();
            int size = 0;
            Text fileName = new Text(((FileSplit) context.getInputSplit()).getPath().getName());

            while (itr.hasMoreTokens()) {
                itr.nextToken();
                size++;
            }

            map.put(fileName, one);
            context.write(new LongWritable(size), new ObjD(map));
        }
    }

    public static class Partitioner extends org.apache.hadoop.mapreduce.Partitioner<LongWritable, ObjD> {

        @Override
        public int getPartition(LongWritable size, ObjD val, int i) {
            return (int) (size.get() % i);
        }
    }

    public static class Reduce extends Reducer<LongWritable, ObjD, LongWritable, ObjD> {
        private final java.util.Map<Text, LongWritable> map = new HashMap<>();

        public void reduce(LongWritable size, Iterable<ObjD> values, Context context) throws IOException, InterruptedException {
            map.clear();

            for (ObjD val : values) {
                for (Text k : val.getMap().keySet()) {
                    if (map.containsKey(k)) {
                        long count = map.get(k).get();
                        map.put(new Text(k), new LongWritable(count + val.getMap().get(k).get()));
                    } else {
                        map.put(new Text(k), new LongWritable(val.getMap().get(k).get()));
                    }
                }
            }

            context.write(size, new ObjD(map));
        }
    }
}
