package MapRed;

import InOut.JoinKey;
import InOut.ObjB;
import InOut.ObjC;
import Tools.CacheData;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static Tools.Tools.*;


public class Join {
    public static long limitByReducer;
    public static long reducer_memory;
    public static float filling_coef;
    public static float minThreshold;
    public static Text inner_file = null;
    public static boolean selfJoin;
    public static String hdfs_port;
    public static String dist;
    public static boolean useLimit;
    public static int step;


    public static class Map extends Mapper<Object, Text, JoinKey, ObjB> {
        private final HashMap<Character, List<String>> set = new HashMap<>();
        private HashMap<Integer, CacheData> cache = new HashMap<>();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString());
            char cle;
            StringBuilder sb;
            ArrayList<String> list_mot = new ArrayList<>();
            Text fileName = new Text(((FileSplit) context.getInputSplit()).getPath().getName());
            if (inner_file == null) {
                inner_file = fileName;
            }
            set.clear();
            int len_set = 0;
            while (itr.hasMoreTokens()) {
                sb = new StringBuilder(itr.nextToken());
                cle = sb.charAt(0);
                len_set++;
                list_mot.clear();

                if (set.containsKey(cle)) {
                    list_mot = (ArrayList<String>) set.get(cle);
                }
                list_mot.add(sb.toString());
                set.put(cle, new ArrayList<>(list_mot));
            }
            int min = getIndex(dist, len_set, minThreshold);

            try {
                cache = getCache(context, hdfs_port, min, len_set);
            } catch (URISyntaxException e) {
                e.printStackTrace();
                System.exit(66);
            }

            for (int i = min; i <= getMax(len_set, step); i++) {
                if (i % step == 0) {
                    if (cache.containsKey(i)) {
                        long limit;
                        if (!useLimit) {
                            limit = (long) Math.ceil((filling_coef * reducer_memory) / cache.get(i).getMemory().get());
                        } else {
                            limit = limitByReducer;
                        }
                        if (!selfJoin) {
                            limit /= 2;
                        }
                        int r_count = cache.get(i).getCount(fileName);
                        int r_buckets = (int) Math.ceil((float) r_count / (float) limit);


                        int s_buckets;
                        int s_count;
                        if (!selfJoin) {
                            s_count = cache.get(i).getOtherCount(fileName);
                            s_buckets = (int) Math.ceil((float) s_count / (float) limit);
                        } else {
                            s_count = r_count;
                            s_buckets = r_buckets;
                        }

                        // Si R et S tiennent sur un seul reducer
                        if (r_count < limit && s_count < limit) {
                            context.write(new JoinKey(new LongWritable(i), new LongWritable(0)), new ObjB(set, fileName, new IntWritable(len_set)));
                        }
                        // S'il faut découper R
                        else if (r_count >= limit && s_count < limit) {
                            int x = (int) (Math.random() * r_buckets);
                            context.write(new JoinKey(new LongWritable(i), new LongWritable(x)), new ObjB(set, fileName, new IntWritable(len_set)));
                        }
                        // S'il faut dupliquer R
                        else if (r_count < limit) {
                            for (int y = 0; y < s_buckets; y++) {
                                context.write(new JoinKey(new LongWritable(i), new LongWritable(y)), new ObjB(set, fileName, new IntWritable(len_set)));
                            }
                        }
                        // S'il faut découper et dupliquer R et S
                        else {
                            int xr = (int) (Math.random() * r_buckets);
                            if (r_buckets > s_buckets) {
                                for (int y = 0; y < s_buckets; y++) {
                                    context.write(new JoinKey(new LongWritable(i), new LongWritable(r_buckets * y + xr)), new ObjB(set, fileName, new IntWritable(len_set)));
                                }

                            } else if (r_buckets < s_buckets) {
                                for (int y = 0; y < s_buckets; y++) {
                                    context.write(new JoinKey(new LongWritable(i), new LongWritable(xr * s_buckets + y)), new ObjB(set, fileName, new IntWritable(len_set)));
                                }
                            } else {
                                if (!selfJoin) {
                                    if (fileName.equals(inner_file)) {
                                        for (int y = 0; y < s_buckets; y++) {
                                            context.write(new JoinKey(new LongWritable(i), new LongWritable(r_buckets * y + xr)), new ObjB(set, fileName, new IntWritable(len_set)));
                                        }
                                    } else {
                                        for (int y = 0; y < s_buckets; y++) {
                                            context.write(new JoinKey(new LongWritable(i), new LongWritable(xr * r_buckets + y)), new ObjB(set, fileName, new IntWritable(len_set)));
                                        }
                                    }
                                } else {
                                    for (int y = 0; y < s_buckets; y++) {
                                        context.write(new JoinKey(new LongWritable(i), new LongWritable(r_buckets * y + xr)), new ObjB(set, fileName, new IntWritable(len_set)));
                                        if ((r_buckets * y + xr) != (xr * r_buckets + y)) {
                                            context.write(new JoinKey(new LongWritable(i), new LongWritable(xr * r_buckets + y)), new ObjB(set, fileName, new IntWritable(len_set)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static class Partitioner extends org.apache.hadoop.mapreduce.Partitioner<JoinKey, ObjB> {

        @Override
        public int getPartition(JoinKey key, ObjB val, int i) {
            return (int) ((key.getGlobalSize().get() / step) % i);
        }
    }

    public static class Reduce extends Reducer<JoinKey, ObjB, DoubleWritable, ObjC> {
        private final List<HashMap<Character, List<String>>> r_m = new ArrayList<>();
        private final List<HashMap<Character, List<String>>> r_p = new ArrayList<>();
        private final List<HashMap<Character, List<String>>> s_m = new ArrayList<>();
        private final List<HashMap<Character, List<String>>> s_p = new ArrayList<>();
        //private Text R_file;
        private Text S_file = null;

        public void reduce(JoinKey key, Iterable<ObjB> values, Context context) throws IOException, InterruptedException {
            r_m.clear();
            r_p.clear();
            s_m.clear();
            s_p.clear();

            for (ObjB v : values) {
                Text v_file = v.getFile();
                if (v_file.equals(inner_file)) {
                    if (v.getSetSize().get() > key.getGlobalSize().get()) {
                        r_p.add(new HashMap<>(v.getSet()));
                    } else {
                        r_m.add(new HashMap<>(v.getSet()));
                    }
                } else {
                    if (v.getSetSize().get() > key.getGlobalSize().get()) {
                        s_p.add(new HashMap<>(v.getSet()));
                    } else {
                        s_m.add(new HashMap<>(v.getSet()));
                    }
                    if (S_file == null) {
                        S_file = new Text(v_file);
                    }
                }
            }

            double[] res;
            if (selfJoin) {
                for (int i = 0; i < r_m.size(); i++) {
                    res = applyDist(r_m.get(i), r_m.subList(i + 1, r_m.size()), dist, minThreshold);
                    for (int y = 0; y < res.length; y++) {
                        if (res[y] >= minThreshold && res[y] < 1) {
                            context.write(new DoubleWritable(res[y]), new ObjC(inner_file, inner_file, flattenSet(r_m.get(i)), flattenSet(r_m.get(y))));
                        }
                    }

                    res = applyDist(r_m.get(i), r_p, dist, minThreshold);
                    for (int y = 0; y < res.length; y++) {
                        if (res[y] >= minThreshold && res[y] < 1) {
                            context.write(new DoubleWritable(res[y]), new ObjC(inner_file, S_file, flattenSet(r_m.get(i)), flattenSet(r_p.get(y))));
                        }
                    }
                }
            } else {
                for (HashMap<Character, List<String>> mapRm : r_m) {
                    res = applyDist(mapRm, s_p, dist, minThreshold);
                    for (int y = 0; y < res.length; y++) {
                        if (res[y] >= minThreshold && res[y] < 1) {
                            context.write(new DoubleWritable(res[y]), new ObjC(inner_file, S_file, flattenSet(mapRm), flattenSet(s_p.get(y))));
                        }
                    }
                    res = applyDist(mapRm, s_m, dist, minThreshold);
                    for (int y = 0; y < res.length; y++) {
                        if (res[y] >= minThreshold && res[y] < 1) {
                            context.write(new DoubleWritable(res[y]), new ObjC(inner_file, S_file, flattenSet(mapRm), flattenSet(s_m.get(y))));
                        }
                    }

                }

                for (HashMap<Character, List<String>> mapSm : s_m) {
                    res = applyDist(mapSm, r_p, dist, minThreshold);
                    for (int y = 0; y < res.length; y++) {
                        if (res[y] >= minThreshold && res[y] < 1) {
                            context.write(new DoubleWritable(res[y]), new ObjC(inner_file, S_file, flattenSet(r_p.get(y)), flattenSet(mapSm)));
                        }
                    }
                }

            }
        }
    }

    public static class PrintMap extends Reducer<JoinKey, ObjB, JoinKey, ObjB> {

        public void reduce(JoinKey key, Iterable<ObjB> values, Context context) throws IOException, InterruptedException {
            for (ObjB val : values) {
                context.write(key, val);
            }
        }
    }
}

