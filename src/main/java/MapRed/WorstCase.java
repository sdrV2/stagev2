package MapRed;

import InOut.ObjC;
import InOut.WorstCaseMapOutput;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static Tools.Tools.applyDist;


public class WorstCase {
    public static float minThreshold;
    public static Text inner_file = null;
    public static boolean selfJoin;
    public static String dist;

    public static class Map extends Mapper<Object, Text, IntWritable, WorstCaseMapOutput> {
        private static final IntWritable one = new IntWritable(1);
        private final HashMap<Character, List<String>> set = new HashMap<>();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString());
            char cle;
            StringBuilder sb;
            ArrayList<String> list_mot = new ArrayList<>();
            Text fileName = new Text(((FileSplit) context.getInputSplit()).getPath().getName());
            if (inner_file == null) {
                inner_file = fileName;
            }

            set.clear();
            while (itr.hasMoreTokens()) {
                sb = new StringBuilder(itr.nextToken());
                cle = sb.charAt(0);
                list_mot.clear();

                if (set.containsKey(cle)) {
                    list_mot = (ArrayList<String>) set.get(cle);
                }
                list_mot.add(sb.toString());
                set.put(cle, new ArrayList<>(list_mot));
            }
            context.write(one, new WorstCaseMapOutput(fileName, set));
        }
    }

    public static class Reduce extends Reducer<IntWritable, WorstCaseMapOutput, DoubleWritable, ObjC> {
        private final List<WorstCaseMapOutput> r = new ArrayList<>();
        private final List<WorstCaseMapOutput> s = new ArrayList<>();

        public void reduce(IntWritable key, Iterable<WorstCaseMapOutput> values, Context context) throws IOException, InterruptedException {
            for (WorstCaseMapOutput val : values) {
                Text v_file = val.getFileName();
                if (v_file.equals(inner_file)) {
                    r.add(new WorstCaseMapOutput(val));
                } else {
                    s.add(new WorstCaseMapOutput(val));
                }
            }

            if (selfJoin) {
                for (int i = 0; i < r.size(); i++) {
                    for (int j = i; j < r.size(); j++) {
                        double res = applyDist(r.get(i).getSet(), r.get(j).getSet(), dist);
                        if (res >= minThreshold && res < 1) {
                            context.write(new DoubleWritable(res), new ObjC(r.get(i).getFileName(), r.get(j).getFileName(), r.get(i).flattenSet(), r.get(j).flattenSet()));
                        }
                    }
                }
            } else {
                for (WorstCaseMapOutput x : r) {
                    for (WorstCaseMapOutput y : s) {
                        double res = applyDist(x.getSet(), y.getSet(), dist);
                        if (res >= minThreshold && res < 1) {
                            context.write(new DoubleWritable(res), new ObjC(x.getFileName(), y.getFileName(), x.flattenSet(), y.flattenSet()));
                        }
                    }
                }
            }
        }
    }
}
