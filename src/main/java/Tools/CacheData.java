package Tools;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.TreeMap;

public class CacheData implements WritableComparable<CacheData> {
    private IntWritable memory;
    private TreeMap<Text, Integer> count;

    public CacheData() {
    }

    public CacheData(IntWritable memory, TreeMap<Text, Integer> count) {
        this.memory = memory;
        this.count = new TreeMap<>(count);
    }

    @Override
    public String toString() {
        return ";" + memory + ";" + count.toString();
    }

    public IntWritable getMemory() {
        return memory;
    }

    public void setMemory(IntWritable memory) {
        this.memory = memory;
    }

    public TreeMap<Text, Integer> getCount() {
        return count;
    }

    public void setCount(TreeMap<Text, Integer> count) {
        this.count = count;
    }

    public Integer getCount(Text key) {
        return count.get(key);
    }

    public Integer getOtherCount(Text key) {
        for (Text k : count.keySet()) {
            if (!k.equals(key)) {
                return count.get(k);
            }
        }
        return 0;
    }

    public void clear() {
        memory = new IntWritable();
        count.clear();
    }

    @Override
    public int compareTo(CacheData o) {
        return 0;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        memory.write(dataOutput);

        dataOutput.writeInt(count.size());
        for (Text k : count.keySet()) {
            k.write(dataOutput);
            dataOutput.writeInt(count.get(k));
        }
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        clear();

        memory.readFields(dataInput);

        int len = dataInput.readInt();
        for (int i = 0; i < len; i++) {
            Text k = new Text(Text.readString(dataInput));
            int v = dataInput.readInt();
            count.put(k, v);
        }
    }
}
