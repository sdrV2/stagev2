package Tools;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;
import java.util.Random;

public class GenerateData {
    public static float minSetSize;
    public static float maxSetSize;
    public static int numberOfSets;

    public static String[] mots = {"un", "deux", "trois", "dix", "bien", "coucou", "roi", "feux", "histoire",
            "ordinateur", "alarme", "canape", "ballon", "avion", "lama", "france", "chateau", "poulet", "vegan",
            "salade", "sapin", "batiment", "caravane", "vacances", "bientot", "salaire", "travail", "appartement",
            "kangorou", "koala", "python", "java", "mpi", "nvidia", "apple", "mexique", "canada", "brexit", "bizon",
            "viande", "assiette", "couteau", "fourchette", "pelle", "truelle", "béton", "mur", "repas", "sports",
            "loisir", "compétition", "podium", "qualification", "coupe", "médaille", "ampoule", "tesla", "batterie",
            "pile", "pôle", "arctique", "antarctique", "europe", "asie", "océanie", "amérique", "afrique", "soleil",
            "lune", "vénus", "saturne", "pluton", "mars", "jupiter", "mercure", "uranus", "neptune", "terre",
            "hydrogène", "lithium", "sodium", "potassium", "rubidium", "césium", "francium", "béryllium",
            "magnésium", "calcium", "strontium", "baryum", "radium", "scandium", "titane", "vanadium", "chrome",
            "manganèse", "fer", "cobalt", "nickel", "cuivre", "zinc", "galium", "germanium", "arsenic", "sélénium",
            "brome", "krypton", "aluminium", "silicium", "phosphore", "soufre", "chlore", "argon", "bore", "carbon",
            "azote", "oxygène", "fluor", "néon", "hélium", "Yttrium", "zirconium", "niobium", "molybdène", "technétium",
            "ruthénium", "rhodium", "paladium", "argent", "cadmium", "indium", "étain", "antimoine", "tellure", "iode",
            "xénon", "hafnium", "tantale", "tungstène", "rhénium", "osmium", "iridium", "platine", "or", "thallium",
            "plomb", "bismuth", "polonium", "astate", "radon", "rf", "db", "sg", "bh", "hs", "mt", "ds", "rg", "cn",
            "nh", "fl", "mc", "lv", "ts", "og", "la", "ce", "pr", "nd", "pm", "sm", "eu", "gd", "tb", "dy", "ho", "er",
            "tm", "yb", "lu", "ac", "th", "pa", "uranium", "np", "pu", "am", "cm", "bk", "cf", "es", "fm", "md", "no", "lr"};


    public static void main(String[] args) throws FileNotFoundException {
        minSetSize = Float.parseFloat(args[0]);
        maxSetSize = Float.parseFloat(args[1]);
        numberOfSets = Integer.parseInt(args[2]);
        genExample();
        //genGenerated();
    }

    public static void genGenerated() {
        try {
            PrintWriter prT = new PrintWriter("src/main/resources/INPUT/Generated2/R");
            PrintWriter prU = new PrintWriter("src/main/resources/INPUT/Generated2/S");
            StringBuilder lineT = new StringBuilder();
            StringBuilder lineU = new StringBuilder();

            for (int i = 0; i < numberOfSets; i++) {
                lineT.setLength(0);
                lineU.setLength(0);
                int setSize = (int) (minSetSize + (Math.random() * (maxSetSize - minSetSize)));
                for (int j = 0; j < setSize; j++) {
                    int mot = (int) (Math.random() * mots.length);
                    lineT.append(mots[mot]).append(" ");
                }
                prT.print(lineT.append("\n"));

                setSize = (int) (minSetSize + (Math.random() * (maxSetSize - minSetSize)));
                for (int j = 0; j < setSize; j++) {
                    int mot = (int) (Math.random() * mots.length);
                    lineU.append(mots[mot]).append(" ");
                }
                prU.print(lineU.append("\n"));
            }

            prT.close();
            prU.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void genExample() throws FileNotFoundException {
        Random r = new Random();
        String alphabet = "abcdefghijkl";

        PrintWriter prT = new PrintWriter("src/main/resources/INPUT/Test/R");
        PrintWriter prU = new PrintWriter("src/main/resources/INPUT/Test/S");
        StringBuilder lineT = new StringBuilder();
        StringBuilder lineU = new StringBuilder();

        for (int i = 0; i < numberOfSets; i++) {
            lineT.setLength(0);
            lineU.setLength(0);

            int setSize = (int) (minSetSize + (Math.random() * (maxSetSize - minSetSize)));
            for (int j = 0; j < setSize; j++) {
                lineT.append(alphabet.charAt(r.nextInt(alphabet.length()))).append(" ");
            }
            prT.print(lineT.append("\n"));

            setSize = (int) (minSetSize + (Math.random() * (maxSetSize - minSetSize)));
            for (int j = 0; j < setSize; j++) {
                lineU.append(alphabet.charAt(r.nextInt(alphabet.length()))).append(" ");
            }
            prU.print(lineU.append("\n"));
        }
        prT.close();
        prU.close();

    }
}
