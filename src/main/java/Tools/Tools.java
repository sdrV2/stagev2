package Tools;

import Distance.Jaccard;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import static Distance.Cosine.*;
import static Distance.Dice.*;
import static Distance.Jaccard.*;
import static Distance.JaccardFreq.*;
import static Distance.Levenshtein.*;
import static Distance.Overlap.*;

public class Tools {
    public static int max_prefixe = 62;

    public static boolean checkDist(String dist, float min) {
        if (dist.equals("J") || dist.equals("Jf") || dist.equals("L") || dist.equals("D") || dist.equals("C") || dist.equals("O")) {
            return !(min < 0) || !(min > 1);
        }
        return false;
    }

    public static int getIndex(String dist, int size, float minThreshold) {
        switch (dist) {
            case "J":
                return jaccardIndex(size, minThreshold);
            case "Jf":
                return jaccardFreqIndex(size, minThreshold);
            case "D":
                return diceIndex(size, minThreshold);
            case "L":
                return levenshteinIndex(size, minThreshold);
            case "C":
                return cosineIndex(size, minThreshold);
            default:
                return overlapIndex(size, minThreshold);
        }
    }

    public static int[] getIndexes(String dist, int size, float minThreshold) {
        switch (dist) {
            case "J":
                return jaccardIndexes(size, minThreshold);
            case "Jf":
                return jaccardFreqIndexes(size, minThreshold);
            case "D":
                return diceIndexes(size, minThreshold);
            case "L":
                return levenshteinIndexes(size, minThreshold);
            case "C":
                return cosineIndexes(size, minThreshold);
            default:
                return overlapIndexes(size, minThreshold);
        }
    }

    public static Double applyDist(HashMap<Character, List<String>> r, HashMap<Character, List<String>> s, String dist) {
        switch (dist) {
            case "J":
                return JaccardDist(r, s);
            case "Jf":
                return JaccardFreqDist(r, s);
            case "D":
                return DiceDist(r, s);
            case "L":
                return LevenshteinDist(r, s);
            case "C":
                return CosineDist(r, s);
            default:
                return OverlapDist(r, s);
        }
    }

    public static double[] applyDist(HashMap<Character, List<String>> r, List<HashMap<Character, List<String>>> s, String dist, float minThreshold) {
        switch (dist) {
            case "J":
                return Jaccard.JaccardDist(r, s, minThreshold);
            case "Jf":
                return JaccardFreqDist(r, s, minThreshold);
            case "D":
                return DiceDist(r, s, minThreshold);
            case "L":
                return LevenshteinDist(r, s, minThreshold);
            case "C":
                return CosineDist(r, s, minThreshold);
            default:
                return OverlapDist(r, s);
        }
    }

    public static int getMax(int size, int step) {
        if (size % step == 0) {
            return size;
        } else {
            return size + step - (size % step);
        }
    }

    /*
    String (bytes) = 8 * (int) ((((no chars) * 2) + 45) / 8)
    Array bytes = 12 bytes header + element + 4 bytes padding
    HashMap <Character (2 bytes), List> =
    Character possible = 26*2 + 10
    https://dzone.com/articles/java-how-to-calculate-size-of-objects-amp-arrays-b
     */
    public static int estimateMemory(String set, int elements) {
        int res = 8 * ((((set.length()) * 2) + 45) / 8);
        if (elements < max_prefixe) {
            res = res + elements * 18;
        } else {
            res = res + max_prefixe * 18;
        }
        return res;
    }

    public static HashMap<Integer, CacheData> getCache(Mapper.Context context, String hdfs_port, long min, long globalSize) throws IOException, URISyntaxException {
        HashMap<Integer, CacheData> res = new HashMap<Integer, CacheData>();
        URI[] uris = context.getCacheFiles();
        InputStreamReader ir = null;
        BufferedReader data = null;
        FileSystem fs = FileSystem.get(new URI(hdfs_port), context.getConfiguration());
        IntWritable memory;

        for (URI uri : uris) {
            Path p = new Path(uri);
            ir = new InputStreamReader(fs.open(p));
            data = new BufferedReader(ir);
            while (data.ready()) {
                String line = data.readLine();
                String[] split = line.split(";");
                int g_len = Integer.parseInt(new StringBuilder(split[0]).deleteCharAt(split[0].length() - 1).toString());
                if (g_len == -1) {
                    memory = new IntWritable(Integer.parseInt(split[1]));
                    TreeMap<Text, Integer> map = new TreeMap<>();
                    res.put(g_len, new CacheData(memory, map));
                } else if (g_len >= min && g_len < globalSize) {
                    memory = new IntWritable(Integer.parseInt(split[1]));
                    TreeMap<Text, Integer> map = parseMap(split[2]);
                    res.put(g_len, new CacheData(memory, /*reducer,*/ map));
                } else if (g_len >= globalSize) {
                    memory = new IntWritable(Integer.parseInt(split[1]));
                    TreeMap<Text, Integer> map = parseMap(split[2]);
                    res.put(g_len, new CacheData(memory, /*reducer,*/ map));
                    ir.close();
                    data.close();
                    return res;
                }
            }
        }
        assert ir != null;
        ir.close();
        data.close();

        return res;
    }


    public static List<Integer> parseList(String s) {
        List<Integer> res = new ArrayList<>();
        StringBuilder sb = new StringBuilder(s);
        sb.deleteCharAt(sb.length() - 1);
        String[] split = sb.toString().split(",");
        for (String value : split) {
            res.add(Integer.parseInt(new StringBuilder(value).deleteCharAt(0).toString()));
        }
        return res;
    }

    public static TreeMap<Text, Integer> parseMap(String s) {
        TreeMap<Text, Integer> res = new TreeMap<>();
        StringBuilder sb = new StringBuilder(s);
        sb.deleteCharAt(sb.length() - 1);
        String[] split = sb.toString().split(",");
        for (String value : split) {
            String[] split2 = new StringBuilder(value).deleteCharAt(0).toString().split("=");
            res.put(new Text(split2[0]), Integer.parseInt(split2[1]));
        }
        return res;
    }

    public static ArrayList<String> flattenSet(HashMap<Character, List<String>> set) {
        ArrayList<String> res = new ArrayList<>();
        for (Character c : set.keySet()) {
            res.addAll(set.get(c));
        }
        return res;
    }

    public static HashMap<Character, List<String>> createPrefixMapFromString(String s) {
        HashMap<Character, List<String>> res = new HashMap<>();
        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < s.length(); i++) {
            list.clear();
            if (res.containsKey(s.charAt(i))) {
                list = (ArrayList<String>) res.get(s.charAt(i));
                list.add(String.valueOf(s.charAt(i)));
                res.put(s.charAt(i), new ArrayList<>(list));
            } else {
                list.add(String.valueOf(s.charAt(i)));
                res.put(s.charAt(i), new ArrayList<>(list));
            }
        }

        return res;
    }
}
