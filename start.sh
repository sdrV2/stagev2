#!/bin/bash

$HOME/Documents/hadoop-3.2.1/sbin/stop-all.sh
rm -fr /tmp/hadoop-$USER/*
#rm -r $HOME/.m2
hadoop namenode -format
$HOME/Documents/hadoop-3.2.1/sbin/start-dfs.sh
$HOME/Documents/hadoop-3.2.1/sbin/start-yarn.sh
